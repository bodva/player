<?php 
class files{
	private $dir;
	private $files = array();
	private $extensions = array();
	public function setDir($dir='music')
	{
		$this->dir = $dir."/";
		// echo $this->dir."<br/>";
	}
	public function getFiles(){
		$fArray = glob($this->dir."*");
		// echo "<pre>";
		// print_r($fArray);
		// echo "</pre>"; 
		foreach ($fArray as $file) {
			if(is_dir($file)) {
				// echo $file."<br>";
				$this->files[] = $file;
				$this->extensions[] = "dir";
				// $tmp = new files;
				// $tmp->setDir($file);				
				// $this->files = array_merge($this->files, $tmp->getFiles());
			} else {
				if(pathinfo($file,PATHINFO_EXTENSION) == 'mp3'){
					$this->files[] = $file;
					$this->extensions[] = "file";
				}
			}
		}
		return array("files"=>$this->files,"ext"=> $this->extensions);
	}
} ?>