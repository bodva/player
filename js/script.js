var track_id = 0;
$(document).ready(function(){
	var music = document.getElementById('audio-player');
	$(".music-file").live("click",function(){
		var filename = $(this).attr("filename");
		track_id = $(this).attr("track");
		// alert($(this).attr("filename"));
		play(filename);
		return false;
	});
	$(".dir-file").live("click",function(){
		var filename = $(this).attr("filename");
		console.log(filename);
		$.ajax({
			url:'/ajax/do.php',
			data:'act=changefolder&folder='+filename,
			type:"POST",
			success:function(data){
				console.log(data);
				$(".playlist-folder").html(data);
			}
		});
		return false;
	});
	
	$(".addlink").live("click",function(){
		var filename = $(this).prev(".music-file").attr("filename");
		console.log(filename);
		$.ajax({
			url:'/ajax/do.php',
			data:'act=addtoplaylist&filename='+filename,
			type:"POST",
			dataType: "json",
			success:function(data){
				$(".playlist-content").html("&nbsp;");
				console.log(data);
				for (var i = 0; i < data.count; i++) {
					var track = data.list[i]
					console.log(track);
					var html= track.view_name+"<br />";
					$(".playlist-content").append(html);
				};
				// $(".playlist-folder").html(data);
			}
		});
		return false;
	});

	$("#clear-playlist").live("click",function(){
		console.log("clear playlist");
		$.ajax({
			url:'/ajax/do.php',
			data:'act=clearplaylist',
			type:"POST",
			dataType: "json",
			success:function(data){
				console.log(data);
				console.log(data.playlist_status);
				if (data.playlist_status == "clean") {
					$(".playlist-content").html("&nbsp;");
				} else {
					alert("clean error");
				}
				// $(".playlist-folder").html(data);
			}
		});
		return false;
	});
});
	function play(trackName){
		var music = document.getElementById('audio-player');
		music.setAttribute('src', trackName);
		console.log(trackName);
		$.ajax({
			url:'/ajax/do.php',
			data:'act=getinfo&filename='+trackName,
			type:"POST",
			dataType: "json",
			success:function(data){
				console.log(data);
				$("#now").html(data.view_name);
			}
		});
		// $("#now").html(trackName);
		music.load();
		music.play();
	}
	function getNextTrack(){
		return "/music/The Beatles - Help!.mp3";
	}

	function playController(){
		track_id++;
		console.log(track_id);
		next_class = $("#track-"+track_id).attr("class");
		console.log(next_class);
		while (next_class == "dir-file") {
			track_id++;
			next_class = $("#track-"+track_id).attr("class");
			console.log(track_id);
			console.log(next_class);
		}
		var filename = $("#track-"+track_id).attr("filename");
		play(filename);
	}