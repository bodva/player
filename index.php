<html>
<head>
<title>Audio Player</title>
<meta> 
<script src="/js/jquery.js"></script>
<script src="/js/script.js"></script>
<link href="/css/style.css" type="text/css" rel="Stylesheet" />
</head>
<script type="text/javascript">
</script>
<body>
<?php 
	include 'files.php';
	$files = new files();
	$files->setDir("music");
	$fArray = $files->getFiles();
?>
<div>
	<div id="now"></div>
	<audio id="audio-player" src="" controls="controls" type="audio/mp3" onended="playController()"></audio><br/>
	<!-- <input type="button" value="play" name="play" id="play"> -->
	<!-- <input type="button" value="play-2" name="play" id="play-2"> -->
</div>
<div class="playlist-folder">
	<?php $ajax = false; ?>
	<?php include 'inc/folder-list.php' ?>
	<?php include 'inc/file-list.php' ?>
</div>
<div class="playlist">
	<div class="playlist-title">Playlist:</div>
	<div class="playlist-content">&nbsp;</div>
</div>
<div class="playlist-options">
	<a href="#" id="clear-playlist">Clear</a>
</div>
</body>
</html>